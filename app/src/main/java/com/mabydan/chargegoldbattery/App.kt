package com.mabydan.chargegoldbattery

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.onesignal.OneSignal
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        val config = YandexMetricaConfig.newConfigBuilder("b250be34-bc25-425f-9a32-d6fef81d07e6").build()
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(applicationContext, config)
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this)

        val conversionListener: AppsFlyerConversionListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
                conversionData.forEach {
                    Log.i("ChargeBattery", "${it.key} ${it.value}")
                }
            }
            override fun onConversionDataFail(errorMessage: String) {
                Log.i("ChargeBattery", "onConversionDataFail $errorMessage")
            }
            override fun onAppOpenAttribution(attributionData: Map<String, String>) {
                attributionData.forEach {
                    Log.i("ChargeBattery", "${it.key} ${it.value}")
                }
            }
            override fun onAttributionFailure(errorMessage: String) {
                Log.i("ChargeBattery", "onAttributionFailure $errorMessage")
            }
        }
        AppsFlyerLib.getInstance().init("jzvY5PTmYwVkSNoTnySxYj", conversionListener, this)
        AppsFlyerLib.getInstance().startTracking(this)

        // Logging set to help debug issues, remove before releasing your app.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)

        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }
}