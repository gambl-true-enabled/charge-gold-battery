package com.mabydan.chargegoldbattery

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.battery_info_fragment.*

class BatteryFragment : Fragment() {

    private val batteryInfoReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            updateBatteryData(intent)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.battery_info_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadBatterySection()
    }

    private fun loadBatterySection() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_POWER_CONNECTED)
        intentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED)
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED)
        activity!!.registerReceiver(batteryInfoReceiver, intentFilter)
    }

    private fun updateBatteryData(intent: Intent) {
        val present = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false)
        if (present) {
            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            if (level != -1 && scale != -1) {
                val batteryPct = (level / scale.toFloat() * 100f).toInt()
                batteryPctTv.setText("$batteryPct% ")
                val timeOffM = ((level / scale.toFloat()) * 4320).toInt()
                val timeOffH = timeOffM / 60
                val timeOffMM = timeOffM % 60
                screen_off_time.text = "${timeOffH}h ${timeOffMM}m"
                val timeOnM = ((level / scale.toFloat()) * 2160).toInt()
                val timeOnH = timeOnM / 60
                val timeOnMM = timeOnM % 60
                screen_on_time.text = "${timeOnH}h ${timeOnMM}m"
                val timeActiveM = ((level / scale.toFloat()) * 1440).toInt()
                val timeActiveH = timeActiveM / 60
                val timeActiveMM = timeActiveM % 60
                active_use_time.text = "${timeActiveH}h ${timeActiveMM}m"
                circular.setProgress(100 - batteryPct)
            }
            val temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0)
            if (temperature > 0) {
                val temp = temperature.toFloat() / 10f
                tempTv.setText("$temp°C")
                seekBar_temperature.value = temp / 100f
            }
            val voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0)
            if (voltage > 0) {
                voltageTv.setText("$voltage mV")
                seekBar_voltage.value = voltage.toFloat() / 5000f
            }
            val capacity = getBatteryCapacity(context!!)
            if (capacity > 0) {
                capacityTv.setText("$capacity mAh")
                seekBar_current_charge.value = capacity.toFloat() / 5000f
            }
        }
    }

    private fun getBatteryCapacity(ctx: Context): Long {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            val mBatteryManager =
//                ctx.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
//            val chargeCounter =
//                mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER)
//            val capacity =
//                mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
//            if (chargeCounter != null && capacity != null) {
//                return (chargeCounter / capacity * 100f).toLong()
//            }
            var mPowerProfile_: Any? = null

            val POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile"

            try {
                mPowerProfile_ = Class.forName(POWER_PROFILE_CLASS)
                    .getConstructor(Context::class.java).newInstance(context)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                val batteryCapacity = Class
                    .forName(POWER_PROFILE_CLASS)
                    .getMethod("getAveragePower", String::class.java)
                    .invoke(mPowerProfile_, "battery.capacity") as Double
                return batteryCapacity.toLong()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return 0
    }

}