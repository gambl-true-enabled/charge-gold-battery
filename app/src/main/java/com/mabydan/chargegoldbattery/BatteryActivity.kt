package com.mabydan.chargegoldbattery

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class BatteryActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_battery)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_container,
                BatteryFragment()
            ).commitNow()
    }

}